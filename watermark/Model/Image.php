<?php
class Image {
    private $original_img;
    private $new_img;
    
    public function __construct($image, $tagline) {
        $this->original_img=$image;
        $orig_width = imagesx($image);
        $orig_height = imagesy($image);

        $horizontal = $orig_width >= $orig_height;

        // Calculate banner sizes
        $banner_height = $orig_height * 15/100;
        $banner_font_size = $horizontal ? max(25, $orig_height * 5 / 100) : max(15, $orig_width * 4 / 100);
        $banner_angle = 0;
        $banner_text = $tagline;

        // Create your canvas containing both image and text
        $canvas = imagecreatetruecolor($orig_width, ($orig_height + $banner_height));
        // Allocate A Color For The background
        $bcolor = imagecolorallocate($canvas, 255, 255, 255);
        // Add background colour into the canvas
        imagefilledrectangle($canvas, 0, 0, $orig_width, ($orig_height + $banner_height), $bcolor);

        // Save image to the new canvas
        imagecopyresampled($canvas, image, 0, 0, 0, 0, $orig_width, $orig_height, $orig_width, $orig_height);

        // Tidy up:
        imagedestroy($image);

        // Set Path to Font File
        $font_path = realpath(dirname(__FILE__)).'/fonts/Helvetica.ttf';

        // Allocate A Color For The Text
        $white = imagecolorallocate($canvas, 255, 255, 255);
        $grey = imagecolorallocate($canvas, 128, 128, 128);
        $black = imagecolorallocate($canvas, 0, 0, 0);

        // Add Border
        $border_width = round(($horizontal ? $orig_width : $orig_height * 1)/100);

        imagefilledrectangle($canvas, 0, $orig_height, $orig_width, $orig_height + $banner_height, $black);
        imagefilledrectangle($canvas, $border_width, $orig_height+$border_width, $orig_width-($border_width), $orig_height + $banner_height-($border_width), $white);

        // Get Bounding Box Size
        $text_box = imagettfbbox($banner_font_size, $banner_angle, $font_path, $banner_text);

        // Get your Text Width and Height
        $text_width = $text_box[2]-$text_box[0];
        $text_height = $text_box[7]-$text_box[1];

        // Calculate coordinates of the text
        $x = ($orig_width/2) - ($text_width/2);
        $y = $orig_height + $banner_height + ($horizontal ? ($text_height) : $text_height * 2);

        // Print Text On Image
        imagettftext($canvas, $banner_font_size, 0, $x, $y, $black, $font_path, $banner_text);

        $this->new_img=$canvas;
    }
}

?>