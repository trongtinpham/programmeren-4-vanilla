fric-frac contains:
    The final fric-frac analysis worked out in vanilla PHP. As mentioned in the assignment, the only worked out models are Country, Role, EventTopic, EventCategory and as an extra: Person.
    
fric-frac-exp contains:
    An experimental playground to try out exercises.
    
watermark contains:
    A introductionary project for a internship.