<?php
    include ('../template/header.php');
    $class = 'User';
    $id = $_GET['Id'];
    // echo $id;
    $model = new \ModernWays\FricFrac\Model\User();
    $model->arrayToObject(\ModernWays\FricFrac\Dal\Dal::readOne($class, $id));
    
   if(isset($_POST['uc'])) {
       if ($_POST['uc'] == 'delete') {
            \ModernWays\FricFrac\Dal\Dal::delete($class, $id);
            header("Location: Index.php");
       }
    }    
?>
<main>
    <article>
        <header>
            <h2>Gebruiker</h2>
        <nav>
            <a href="UpdatingOne.php?Id=<?= $model->getId();?>">Updating</a>
            <a href="InsertingOne.php">Inserting</a>
            <button type="submit" name="uc" value="delete" form="form">Delete</button>
            <a href="Index.php">Annuleren</a>
        </nav>
        </header>
        <form id="form" method="post">
            <ul class="form-style-1">
                <li>
                    <label for="name">Naam</label>
                    <input type="text" name="Name" id="name" readonly
                        value="<?= $model->getName();?>">
                </li>
                <li>
                    <label for="salt">Zout</label>
                    <input type="text" name="Salt" id="salt" readonly
                        value="<?= $model->getSalt();?>">
                </li>
                <li>
                    <label for="hashedPassword">Hash</label>
                    <input type="text" name="HashedPassword" id="hashedPassword" readonly
                        value="<?= $model->getHashedPassword();?>">
                </li>
                <li>
                    <label for="person">Persoon</label>
                    <input type="text" name="Person" id="person" readonly
                        value="<?= $model->getPersonName();?>">
                </li>
                <li>
                    <label for="role">Rol</label>
                    <input type="text" name="Role" id="role" readonly
                        value="<?= $model->getRoleName();?>">
                </li>
            </ul>
        </form>
        <div id="feedback"></div>

    </article>
    <?php include('ReadingAll.php');?>
</main>
<?php include('../template/footer.php');?>