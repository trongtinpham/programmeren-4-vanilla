<?php
    include ('../template/header.php');
    $class = 'User';
    $id = $_GET['Id'];
    $model = new \ModernWays\FricFrac\Model\User();
    $model->arrayToObject(\ModernWays\FricFrac\Dal\Dal::readOne($class, $id));

   if(isset($_POST['uc'])) {
        $model->arrayToObject($_POST);
        \ModernWays\FricFrac\Dal\Dal::update($class, $model->toArray());
        header("Location: ReadingOne.php?Id={$id}");
    }?>
<main>
    <article>
        <header>
            <h2>Gebruiker</h2>
        <nav>
            <button type="submit" name="uc" value="update" form="form">Update</button>
            <a href="ReadingOne.php?Id=<?= $id ?>">Annuleren</a>
        </nav>
        </header>
        <form id="form" action="" method="POST">
            <ul class="form-style-1">
                <li>
                    <label for="name">Name</label>
                    <input type="text" name="Name" id="name" required value="<?= $model->getName();?>">
                </li>
                <li>
                    <label for="salt">Zout</label>
                    <input type="text" name="Salt" id="salt" value="<?= $model->getSalt();?>">
                </li>
                <li>
                    <label for="hashedPassword">Hash</label>
                    <input type="text" name="HashedPassword" id="hashedPassword" required value="<?= $model->getHashedPassword();?>">
                </li>
                <li>
                    <label for="Person">Persoon</label>
                    <select name="PersonId" required>
                        <?php $default = \ModernWays\FricFrac\Dal\Dal::ReadOne('Person', $model->getPersonId()); ?>
                            <option value="<?= $model->getPersonId() ?>" selected><?= $default['FirstName'] ?> <?= $default['LastName'] ?></option>
                        <?php
                        $persons = \ModernWays\FricFrac\Dal\Dal::ReadAll('Person', 'FirstName', ['Id', 'FirstName', 'LastName']);
                        $users = \ModernWays\FricFrac\Dal\Dal::ReadAll($class, 'PersonId', ['PersonId']);
                        $users = array_map('end', $users);
                        foreach ($persons as $person) :
                            if (!in_array($person['Id'], $users)) : ?>
                                <option value="<?= $person['Id'];?>"><?= $person['FirstName']?> <?= $person['LastName']?></option>
                            <?php endif;
                        endforeach; ?>
                    </select>
                </li>
                <li>
                    <label for="roleId">Rol</label>
                    <select name="RoleId">
                        <?php
                        $s = \ModernWays\FricFrac\Dal\Dal::ReadAll('Role');
                        foreach ($s as $role) : ?>
                            <option value="<?= $role['Id'];?>" <?= $role['Id'] == $model->getRoleId() ? 'selected="selected"' : ''; ?>><?= $role['Name']?></option>
                        <?php endforeach; ?>
                    </select>
                </li>
            </ul>
       </form>
        <div id="feedback"></div>

    </article>
    <?php include('ReadingAll.php');?>
</main>
<?php include('../template/footer.php');?>