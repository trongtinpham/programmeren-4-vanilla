
<?php
    include ('../template/header.php');
    $class = 'User';
    
    if(isset($_POST['uc'])) {
        $model = new \ModernWays\FricFrac\Model\User();
        $model->arrayToObject($_POST);
        \ModernWays\FricFrac\Dal\Dal::create($class, $model->toArray());
        header("Location: Index.php");
    }
?>
<main>
    <article>
        <nav>
            <button type="submit" name="uc" value="insert" form="form">Insert</button>
            <a href="Index.php">Annuleren</a>
        <form id="form" method="post">
            <ul class="form-style-1">
                <li><label for="name">Naam</label><input type="text" name="Name" id="name" required></li>
                <li><label for="salt">Zout</label><input type="text" name="Salt" id="salt"></li>
                <li><label for="hashedPassword">Hash</label><input type="text" name="HashedPassword" id="hashedPassword" required></li>
                <li><label for="personId">Persoon</label>
                    <select name="PersonId" required>
                        <option value=""></option>
                        <?php
                        $persons = \ModernWays\FricFrac\Dal\Dal::ReadAll('Person', 'FirstName', ['Id', 'FirstName', 'LastName']);
                        $users = \ModernWays\FricFrac\Dal\Dal::ReadAll($class, 'PersonId', ['PersonId']);
                        $users = array_map('end', $users);
                        foreach ($persons as $person) :
                            if (!in_array($person['Id'], $users)) : ?>
                                <option value="<?= $person['Id'];?>"><?= $person['FirstName']?> <?= $person['LastName']?></option>
                            <?php endif;
                        endforeach; ?>
                    </select>
                </li>
                <li><label for="roleId">Rol</label>
                    <select name="RoleId">
                        <?php
                        $s = \ModernWays\FricFrac\Dal\Dal::ReadAll('Role');
                        foreach ($s as $role) : ?>
                            <option value="<?= $role['Id'];?>"><?= $role['Name']?></option>
                        <?php endforeach; ?>
                    </select>
                </li>
            </ul>
        </form>
        <div id="feedback"></div>
    </article>
    <?php include('ReadingAll.php'); ?>
</main>
<?php include('../template/footer.php');?>