<?php
    $class = 'User';
    $list = \ModernWays\FricFrac\Dal\Dal::readAll($class, 'Name');
?>
<aside>
    <table>
        <?php if ($list) : ?>
            <tr>
                <th>Select</th>
                <th>Naam</th>
                <th>Zout</th>
                <th>Hash</th>
            </tr>
            <?php foreach($list as $item) : ?>
                <tr>
                    <td><a href="ReadingOne.php?Id=<?= $item['Id'];?>">-></a></td>
                    <td><?= $item['Name'];?></td>
                    <td><?= $item['Salt'];?></td>
                    <td><?= $item['HashedPassword'];?></td>
                </tr>
            <?php endforeach;
        else : ?>
            <tr><td>Geen gebruikers gevonden</td></tr>
        <?php endif; ?>
    </table>
</aside>
