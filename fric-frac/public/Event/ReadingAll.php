<?php
    $class = 'Event';
    $list = \ModernWays\FricFrac\Dal\Dal::readAll($class);
?>
<aside>
    <table>
        <?php if ($list) : ?>
            <tr>
                <th>Select</th>
                <th>Naam</th>
                <th>Locatie</th>
                <th>Afbeelding</th>
                <th>Beschrijving</th>
                <th>Organisator naam</th>
                <th>Organisator beschrijving</th>
            </tr>
            <?php foreach($list as $item) : ?>
                <tr>
                    <td><a href="ReadingOne.php?Id=<?= $item['Id'];?>">-></a></td>
                    <td><?= $item['Name'];?></td>
                    <td><?= $item['Location'];?></td>
                    <td><?= $item['Image'];?></td>
                    <td><?= $item['Description'];?></td>
                    <td><?= $item['OrganiserName'];?></td>
                    <td><?= $item['OrganiserDescription'];?></td>
                </tr>
            <?php endforeach;
        else : ?>
            <tr><td>Geen events gevonden</td></tr>
        <?php endif; ?>
    </table>
</aside>
