
<?php
    include ('../template/header.php');
    $class = 'Event';
    
    if(isset($_POST['uc'])) {
        $model = new \ModernWays\FricFrac\Model\Event();
        $model->arrayToObject($_POST);
        \ModernWays\FricFrac\Dal\Dal::create($class, $model->toArray());
        header("Location: Index.php");
    }
?>
<main>
    <article>
        <nav>
            <button type="submit" name="uc" value="insert" form="form">Insert</button>
            <a href="Index.php">Annuleren</a>
        <form id="form" method="post">
            <ul class="form-style-1">
                <li>
                    <label for="name">Naam</label>
                    <input type="text" name="Name" id="name" required></li>
                <li>
                    <label for="location">Locatie</label
                    input type="text" name="Location" id="location" required></li>
                <li>
                    <label for="starts">Start</label>
                    <input type="date" name="Starts" id="starts"></li>
                <li>
                    <label for="ends">Einde</label>
                    <input type="date" name="Ends" id="ends"></li>
                <li>
                    <label for="image">Afbeelding</label>
                    <input type="file" name="Image" id="image" accept="image/*" required></li>
                <li>
                    <label for="description">Omschrijving</label>
                    <input type="text" name="Description" id="description" required></li>
                <li>
                    <label for="organiserName">Organisator naam</label>
                    <input type="text" name="OrganiserName" id="organiserName" required></li>
                <li>
                    <label for="organiserDescription">Organisator beschrijving</label>
                    <input type="text" name="OrganiserDescription" id="organiserDescription" required></li>
                <li>
                    <label for="eventCategoryId">Event categorie</label>
                    <select name="EventCategoryId">
                        <?php
                        $c = \ModernWays\FricFrac\Dal\Dal::ReadAll('EventCategory');
                        foreach ($c as $cat) : ?>
                            <option value="<?= $cat['Id'];?>"><?= $cat['Name']?></option>
                        <?php endforeach; ?>
                    </select>
                </li>
                <li>
                    <label for="eventTopicId">Event topic</label>
                    <select name="EventTopicId">
                        <?php
                        $t = \ModernWays\FricFrac\Dal\Dal::ReadAll('EventTopic');
                        foreach ($t as $topic) : ?>
                            <option value="<?= $topic['Id'];?>"><?= $topic['Name']?></option>
                        <?php endforeach; ?>
                    </select>
                </li>
            </ul>
        </form>
        <div id="feedback"></div>
    </article>
    <?php include('ReadingAll.php'); ?>
</main>
<?php include('../template/footer.php');?>