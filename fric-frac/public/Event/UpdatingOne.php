<?php
    include ('../template/header.php');
    $class = 'Event';
    $id = $_GET['Id'];
    $model = new \ModernWays\FricFrac\Model\Event();
    $model->arrayToObject(\ModernWays\FricFrac\Dal\Dal::readOne($class, $id));

   if(isset($_POST['uc'])) {
        $model->arrayToObject($_POST);
        \ModernWays\FricFrac\Dal\Dal::update($class, $model->toArray());
        header("Location: ReadingOne.php?Id={$id}");
    }?>
<main>
    <article>
        <header>
            <h2>Event</h2>
        <nav>
            <button type="submit" name="uc" value="update" form="form">Update</button>
            <a href="ReadingOne.php?Id=<?= $id ?>">Annuleren</a>
        </nav>
        </header>
        <form id="form" action="" method="POST">
            <ul class="form-style-1">
                <li>
                    <label for="name">Naam</label>
                    <input type="text" name="Name" id="name" required value="<?= $model->getName() ?>"></li>
                <li>
                    <label for="location">Locatie</label
                    input type="text" name="Location" id="location" required value="<?= $model->getLocation() ?>"></li>
                <li>
                    <label for="starts">Start</label>
                    <input type="date" name="Starts" id="starts" value="<?= $model->getStarts() ?>"></li>
                <li>
                    <label for="ends">Einde</label>
                    <input type="date" name="Ends" id="ends" value="<?= $model->getEnds() ?>"></li>
                <li>
                    <label for="image">Afbeelding</label>
                    <input type="file" name="Image" id="image" accept="image/*" required></li>
                <li>
                    <label for="description">Omschrijving</label>
                    <input type="text" name="Description" id="description" required value="<?= $model->getDescription() ?>"></li>
                <li>
                    <label for="organiserName">Organisator naam</label>
                    <input type="text" name="OrganiserName" id="organiserName"  value="<?= $model->getOrganiserName() ?>"></li>
                <li>
                    <label for="organiserDescription">Organisator beschrijving</label>
                    <input type="text" name="OrganiserDescription" id="organiserDescription" required value="<?= $model->getOrganiserDescription() ?>"></li>
                <li>
                    <label for="eventCategoryId">Event categorie</label>
                    <select name="EventCategoryId">
                        <?php
                        $c = \ModernWays\FricFrac\Dal\Dal::ReadAll('EventCategory');
                        foreach ($c as $cat) : ?>
                            <option value="<?= $cat['Id'];?>" <?= $cat['Id'] == $model->getEventCategoryId() ? 'selected="selected"' : ''; ?>><?= $cat['Name']?></option>
                        <?php endforeach; ?>
                    </select>
                </li>
                <li>
                    <label for="eventTopicId">Event topic</label>
                    <select name="EventTopicId">
                        <?php
                        $t = \ModernWays\FricFrac\Dal\Dal::ReadAll('EventTopic');
                        foreach ($t as $topic) : ?>
                            <option value="<?= $topic['Id'];?>" <?= $topic['Id'] == $model->getEventTopicId() ? 'selected="selected"' : ''; ?>><?= $topic['Name']?></option>
                        <?php endforeach; ?>
                    </select>
                </li>
            </ul>
       </form>
        <div id="feedback"></div>

    </article>
    <?php include('ReadingAll.php');?>
</main>
<?php include('../template/footer.php');?>