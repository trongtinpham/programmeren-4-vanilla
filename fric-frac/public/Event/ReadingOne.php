<?php
    include ('../template/header.php');
    $class = 'Event';
    $id = $_GET['Id'];
    // echo $id;
    $model = new \ModernWays\FricFrac\Model\Event();
    $model->arrayToObject(\ModernWays\FricFrac\Dal\Dal::readOne($class, $id));
    
   if(isset($_POST['uc'])) {
       if ($_POST['uc'] == 'delete') {
            \ModernWays\FricFrac\Dal\Dal::delete($class, $id);
            header("Location: Index.php");
       }
    }    
?>
<main>
    <article>
        <header>
            <h2>Event</h2>
        <nav>
            <a href="UpdatingOne.php?Id=<?= $model->getId();?>">Updating</a>
            <a href="InsertingOne.php">Inserting</a>
            <button type="submit" name="uc" value="delete" form="form">Delete</button>
            <a href="Index.php">Annuleren</a>
        </nav>
        </header>
        <form id="form" method="post">
            <ul class="form-style-1">
                <li>
                    <label for="name">Naam</label>
                    <input type="text" name="Name" id="name" readonly value="<?= $model->getName();?>">
                </li>
                <li>
                    <label for="location">Locatie</label>
                    <input type="text" name="Location" id="location" readonly value="<?= $model->getLocation();?>">
                </li>
                <li>
                    <label for="starts">Start</label>
                    <input type="text" name="Starts" id="starts" readonly value="<?= $model->getStarts();?>">
                </li>
                <li>
                    <label for="ends">Einde</label>
                    <input type="text" name="Ends" id="ends" readonly value="<?= $model->getEnds();?>">
                </li>
                <li>
                    <label for="image">Afbeelding</label>
                    <input type="text" name="Image" id="image" readonly value="<?= $model->getImage();?>">
                </li>
                <li>
                    <label for="description">Beschrijving</label>
                    <input type="text" name="Description" id="description" readonly value="<?= $model->getDescription();?>">
                </li>
                <li>
                    <label for="organiserName">Organisator naam</label>
                    <input type="text" name="OrganiserName" id="organiserName" readonly value="<?= $model->getOrganiserName();?>">
                </li>
                <li>
                    <label for="organiserDescription">Organisator beschrijving</label>
                    <input type="text" name="OrganiserDescription" id="organiserDescription" readonly value="<?= $model->getOrganiserDescription();?>">
                </li>
            </ul>
        </form>
        <div id="feedback"></div>

    </article>
    <?php include('ReadingAll.php');?>
</main>
<?php include('../template/footer.php');?>