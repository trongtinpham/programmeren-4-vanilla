<?php
    $class = 'Country';
    $list = \ModernWays\FricFrac\Dal\Dal::readAll($class);
?>
<aside>
    <table>
        <?php if ($list) : ?>
            <tr>
                <th>Select</th>
                <th>Naam</th>
                <th>Code</th>
            </tr>
            <?php foreach($list as $item) : ?>
                <tr>
                    <td><a href="ReadingOne.php?Id=<?= $item['Id'];?>">-></a></td>
                    <td><?= $item['Name'];?></td>
                    <td><?= $item['Code'];?></td>
                </tr>
            <?php endforeach;
        else : ?>
            <tr><td>Geen landen gevonden</td></tr>
        <?php endif; ?>
    </table>
</aside>
