
<?php
    include ('../template/header.php');
    $class = 'Country';
    if(isset($_POST['uc'])) {
        $model = new \ModernWays\FricFrac\Model\Country();
        $model->arrayToObject($_POST);
        \ModernWays\FricFrac\Dal\Dal::create($class, $model->toArray());
    }
?>
<main>
    <article>
        <header>
            <h2>Land</h2>
        <nav>
            <button type="submit" name="uc" value="insert" form="form">Insert</button>
            <a href="Index.php">Annuleren</a>
        </nav>
        </header>
        <form id="form" action="" method="POST">
            <ul class="form-style-1">
                <li>
                    <label for="Name">Naam</label>
                    <input type="text" required id="Name" name="Name"/>
                </li>
                 <li>
                    <label for="Code">Code</label>
                    <input type="text" required id="Code" name="Code"/>
                </li>
            </ul>
       </form>
        <div id="feedback"></div>

    </article>
    <?php include('ReadingAll.php');?>
</main>
<?php include('../template/footer.php');?>