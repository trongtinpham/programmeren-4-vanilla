<?php
    $class = 'EventTopic';
    $list = \ModernWays\FricFrac\Dal\Dal::readAll($class);
?>
<aside>
    <table>
        <?php if ($list) : ?>
            <tr>
                <th>Select</th>
                <th>Naam</th>
            </tr>
            <?php foreach($list as $item) : ?>
                <tr>
                    <td><a href="ReadingOne.php?Id=<?= $item['Id'];?>">-></a></td>
                    <td><?= $item['Name'];?></td>
                </tr>
            <?php endforeach;
        else : ?>
            <tr><td>Geen event topics gevonden</td></tr>
        <?php endif; ?>
    </table>
</aside>
