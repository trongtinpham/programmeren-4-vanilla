<?php
    include ('../template/header.php');
    $class = 'EventTopic';
    $id = $_GET['Id'];
    $model = new \ModernWays\FricFrac\Model\EventCategory();
    $model->arrayToObject(\ModernWays\FricFrac\Dal\Dal::readOne($class, $id));

   if(isset($_POST['uc'])) {
        $model->arrayToObject($_POST);
        \ModernWays\FricFrac\Dal\Dal::update($class, $model->toArray());
        header("Location: ReadingOne.php?Id={$id}");
    }?>
<main>
    <article>
        <header>
            <h2>Event categorie</h2>
        <nav>
            <button type="submit" name="uc" value="update" form="form">Update</button>
           <a href="Index.php">Annuleren</a>
        </nav>
        </header>
        <form id="form" action="" method="POST">
            <ul class="form-style-1">
                <li>
                    <label for="Name">Naam</label>
                    <input type="text" required id="Name" name="Name" 
                        value="<?= $model->getName();?>"/>
                </li>
            </ul>
       </form>
        <div id="feedback"></div>

    </article>
    <?php include('ReadingAll.php');?>
</main>
<?php include('../template/footer.php');?>