<?php
    include ('../template/header.php');
    $class = 'EventCategory';
    $id = $_GET['Id'];
    
    $model = new \ModernWays\FricFrac\Model\EventCategory();
    $model->arrayToObject(\ModernWays\FricFrac\Dal\Dal::readOne($class, $id));
    
   if(isset($_POST['uc'])) {
       if ($_POST['uc'] == 'delete') {
            \ModernWays\FricFrac\Dal\Dal::delete($class, $id);
            header("Location: Index.php");
       }
    }    
?>
<main>
    <article>
        <header>
            <h2>Event categorie</h2>
        <nav>
            <a href="UpdatingOne.php?Id=<?= $model->getId();?>">Updating</a>
            <a href="InsertingOne.php">Inserting</a>
            <button type="submit" name="uc" value="delete" form="form">Delete</button>
            <a href="Index.php">Annuleren</a>
        </nav>
        </header>
        <form id="form" action="" method="post">
            <ul class="form-style-1">
                <li>
                    <label for="Name">Naam</label>
                    <input type="text" readonly id="Name" name="Name" readonly
                        value="<?= $model->getName();?>"/>
                </li>
            </ul>
        </form>
        <div id="feedback"></div>

    </article>
    <?php include('ReadingAll.php');?>
</main>
<?php include('../template/footer.php');?>