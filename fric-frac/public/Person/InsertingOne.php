
<?php
    include ('../template/header.php');
    $class = 'Person';
    if(isset($_POST['uc'])) {
        $model = new \ModernWays\FricFrac\Model\Person();
        $model->arrayToObject($_POST);
        // var_dump($_POST);
        \ModernWays\FricFrac\Dal\Dal::create($class, $model->toArray());
    }
?>
<main>
    <article>
        <nav>
            <button type="submit" name="uc" value="insert" form="form">Insert</button>
            <a href="Index.php">Annuleren</a>
        <form id="form" method="post">
            <ul class="form-style-1">
            <li><label for="firstName">Voornaam</label><input type="text" name="FirstName" id="firstName" required></li>
            <li><label for="lastName">Achternaam</label><input type="text" name="LastName" id="lastName" required></li>
            <li><label for="email">Email</label><input type="text" name="Email" id="email" required></li>
            <li><label for="Address1">Address 1</label><input type="text" name="Address1" id="Address1"></li>
            <li><label for="Address2">Address 2</label><input type="text" name="Address2" id="Address2"></li>
            <li><label for="PostalCode">Postcode</label><input type="text" name="PostalCode" id="PostalCode"></li>
            <li><label for="City">Stad</label><input type="text" name="City" id="City"></li>
            <li><label for="Country">Land</label>
                <select name="CountryId">
                    <?php
                    $s = \ModernWays\FricFrac\Dal\Dal::ReadAll('Country');
                    foreach ($s as $cntr){
                        ?>
                        <option value="<?= $cntr['Id'];?>"><?= $cntr['Name']?></option>
                        <?php
                    }
                    ?>
                </select>
            </li>
            <li><label for="Phone">Telefoon</label><input type="tel" name="Phone1" id="Phone"></li>
            <li><label for="birthday">Geboorte datum</label><input type="date" name="Birthday" id="birthday"></li>
            </ul>
        </form>
        <div id="feedback"></div>
    </article>
    <?php include('ReadingAll.php'); ?>
</main>
<?php include('../template/footer.php');?>