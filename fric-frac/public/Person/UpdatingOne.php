<?php
    include ('../template/header.php');
    $class = 'Person';
    $id = $_GET['Id'];
    $model = new \ModernWays\FricFrac\Model\Person();
    $model->arrayToObject(\ModernWays\FricFrac\Dal\Dal::readOne($class, $id));

   if(isset($_POST['uc'])) {
        $model->arrayToObject($_POST);
        \ModernWays\FricFrac\Dal\Dal::update($class, $model->toArray());
    }?>
<main>
    <article>
        <header>
            <h2>Persoon</h2>
        <nav>
            <button type="submit" name="uc" value="update" form="form">Update</button>
            <a href="ReadingOne.php?Id=<?= $id ?>">Annuleren</a>
        </nav>
        </header>
        <form id="form" action="" method="POST">
            <ul class="form-style-1">
                <li>
                    <label for="firstName">Voornaam</label>
                    <input type="text" name="FirstName" id="firstName" required
                        value="<?= $model->getFirstName();?>">
                </li>
                <li>
                    <label for="lastName">Achternaam</label>
                    <input type="text" name="LastName" id="lastName" required
                        value="<?= $model->getLastName();?>">
                </li>
                <li>
                    <label for="email">Email</label>
                    <input type="text" name="Email" id="email" required
                        value="<?= $model->getEmail();?>">
                </li>
                <li>
                    <label for="Address1">Address 1</label>
                    <input type="text" name="Address1" id="Address1"
                        value="<?= $model->getAddress1();?>">
                </li>
                <li>
                    <label for="Address2">Address 2</label>
                    <input type="text" name="Address2" id="Address2"
                        value="<?= $model->getAddress2();?>">
                </li>
                <li>
                    <label for="PostalCode">Postcode</label>
                    <input type="text" name="PostalCode" id="PostalCode"
                        value="<?= $model->getPostalCode();?>">
                </li>
                <li>
                    <label for="City">Stad</label>
                    <input type="text" name="City" id="City"
                        value="<?= $model->getCity();?>">
                </li>
                <li>
                    <label for="Country">Land</label>
                    <select name="CountryId">
                        <?php
                        $s = \ModernWays\FricFrac\Dal\Dal::ReadAll('Country');
                        foreach ($s as $country): ?>
                            <option value="<?= $country['Id'];?>" <?= $country['Id'] == $model->getCountryId() ? 'selected="selected"' : ''; ?>><?= $country['Name']?></option>
                        <?php endforeach; ?>
                    </select>
                </li>
                <li>
                    <label for="Phone">Telefoon</label>
                    <input type="tel" name="Phone1" id="Phone"
                        value="<?= $model->getPhone1();?>">
                </li>
                <li>
                    <label for="Birthday">Geboorte datum</label>
                    <input type="date" name="Birthday" id="Birthday"
                        value="<?= $model->getBirthday();?>">
                </li>
            </ul>
       </form>
        <div id="feedback"></div>

    </article>
    <?php include('ReadingAll.php');?>
</main>
<?php include('../template/footer.php');?>