<?php
    $class = 'Person';
    $list = \ModernWays\FricFrac\Dal\Dal::readAll($class, 'FirstName');
?>
<aside>
    <table>
        <?php if ($list) : ?>
            <tr>
                <th>Select</th>
                <th>Voornaam</th>
                <th>Familienaam</th>
            </tr>
            <?php foreach($list as $item) : ?>
                <tr>
                    <td><a href="ReadingOne.php?Id=<?= $item['Id'];?>">-></a></td>
                    <td><?= $item['FirstName'];?></td>
                    <td><?= $item['LastName'];?></td>
                </tr>
            <?php endforeach;
        else : ?>
            <tr><td>Geen personen gevonden</td></tr>
        <?php endif; ?>
    </table>
</aside>
