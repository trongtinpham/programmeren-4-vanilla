<?php
    include ('../template/header.php');
    $class = 'Person';
    $id = $_GET['Id'];
    // echo $id;
    $model = new \ModernWays\FricFrac\Model\Person();
    $model->arrayToObject(\ModernWays\FricFrac\Dal\Dal::readOne($class, $id));
    
   if(isset($_POST['uc'])) {
       if ($_POST['uc'] == 'delete') {
            \ModernWays\FricFrac\Dal\Dal::delete($class, $id);
            header("Location: Index.php");
       }
    }    
?>
<main>
    <article>
        <header>
            <h2>Persoon</h2>
        <nav>
            <a href="UpdatingOne.php?Id=<?= $model->getId();?>">Updating</a>
            <a href="InsertingOne.php">Inserting</a>
            <button type="submit" name="uc" value="delete" form="form">Delete</button>
            <a href="Index.php">Annuleren</a>
        </nav>
        </header>
        <form id="form" method="post">
            <ul class="form-style-1">
            <li>
                <label for="firstName">Voornaam</label>
                <input type="text" name="FirstName" id="firstName" readonly
                    value="<?= $model->getFirstName();?>">
            </li>
            <li>
                <label for="lastName">Achternaam</label>
                <input type="text" name="LastName" id="lastName" readonly
                    value="<?= $model->getLastName();?>">
            </li>
            <li>
                <label for="email">Email</label>
                <input type="text" name="Email" id="email" readonly
                    value="<?= $model->getEmail();?>">
            </li>
            <li>
                <label for="Address1">Address 1</label>
                <input type="text" name="Address1" id="Address1" readonly
                    value="<?= $model->getAddress1();?>">
            </li>
            <li>
                <label for="Address2">Address 2</label>
                <input type="text" name="Address2" id="Address2" readonly
                    value="<?= $model->getAddress2();?>">
            </li>
            <li>
                <label for="PostalCode">Postcode</label>
                <input type="text" name="PostalCode" id="PostalCode" readonly
                    value="<?= $model->getPostalCode();?>">
            </li>
            <li>
                <label for="City">Stad</label>
                <input type="text" name="City" id="City" readonly
                    value="<?= $model->getCity();?>">
            </li>
            <li>
                <label for="Country">Land</label>
                <input type="text" name="Country" id="Country" readonly
                    value="<?= $model->getCountryName();?>">
            </li>
            <li>
                <label for="Phone">Telefoon</label>
                <input type="tel" name="Phone1" id="Phone1" readonly
                    value="<?= $model->getPhone1();?>">
            </li>
            <li>
                <label for="birthday">Geboorte datum</label>
                <input type="date" name="Birthday" id="birthday" readonly
                    value="<?= $model->getBirthday();?>">
            </li>
            </ul>
        </form>
        <div id="feedback"></div>

    </article>
    <?php include('ReadingAll.php');?>
</main>
<?php include('../template/footer.php');?>