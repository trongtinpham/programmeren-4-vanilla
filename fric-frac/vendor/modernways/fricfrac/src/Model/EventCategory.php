<?php
namespace ModernWays\FricFrac\Model;
use ModernWays\Helpers;

class EventCategory {
    private $Name;
    private $Id;
    private $List;
    
    public function setName($value) {
        $this->Name = \ModernWays\Helpers::cleanUpInput($value);
    }
    
    public function setId($value) {
        $this->Id = \ModernWays\Helpers::cleanUpInput($value);
    }
    
        public function getName() {
        return $this->Name;
    }
    
    public function getId() {
        return $this->Id;
    }
    
    public function setProperty($prop, $value)
    {
        if (property_exists(self::class, $prop)) {
            $method = "set$prop";
            $this->$method($value);
        }
    }

    public function getProperty($prop)
    {
        if (property_exists(self::class, $prop)) {
            $method = "get$prop";
            return $this->$method();
        }
    }
    
    public function toArray() {
        return array(
            "Name" => $this->getName(),
            "Id" => $this->getId());
    }
    
    public function arrayToObject($arr)
    {
        foreach ($arr as $key => $value) {
            $this->setProperty($key, $value);
        }
    }
}