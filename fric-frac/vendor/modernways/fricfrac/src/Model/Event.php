<?php
namespace ModernWays\FricFrac\Model;

class Event {
    private $Name;
    private $Location;
    private $Id;
    private $Image;
    private $Starts;
    private $Ends;
    private $Description;
    private $OrganiserName;
    private $OrganiserDescription;
    private $EventCategoryId;
    private $EventTopicId;
    
    private $list;
    
    public function setId($value) {
        $this->Id = \ModernWays\Helpers::cleanUpInput($value);
    }
    public function setName($value) {
        $this->Name = \ModernWays\Helpers::cleanUpInput($value);
    }
    
    public function setImage($value) {
        $this->Image = \ModernWays\Helpers::cleanUpInput($value);
    }
    
    public function setLocation($value) {
        $this->Location = \ModernWays\Helpers::cleanUpInput($value);
    }
    
    public function setStarts($value) {
        $this->Starts = date('Y-m-d', strtotime($value));;
    }
    
    public function setEnds($value) {
        $this->Ends = date('Y-m-d', strtotime($value));;
    }
    
    public function setDescription($value) {
        $this->Description = \ModernWays\Helpers::cleanUpInput($value);
    }
    
    public function setOrganiserName($value) {
        $this->OrganiserName = \ModernWays\Helpers::cleanUpInput($value);
    }
    
    public function setOrganiserDescription($value) {
        $this->OrganiserDescription = \ModernWays\Helpers::cleanUpInput($value);
    }
    
    public function setEventCategoryId($value) {
        $this->EventCategoryId = \ModernWays\Helpers::cleanUpInput($value);
    }
    
    public function setEventTopicId($value) {
        $this->EventTopicId = \ModernWays\Helpers::cleanUpInput($value);
    }
    
    public function getId() {
        return $this->Id;
    }
    
    public function getName() {
    return $this->Name;
    }
    
    public function getLocation() {
        return $this->Location;
    }
    
    public function getImage() {
        return $this->Image;
    }
     
    public function getStarts() {
        return $this->Starts;
    }
    
    public function getStartDate() {
        $date = new \DateTime($this->Starts);
        return $date->format('Y-m-d');
    }
    
        public function getEndDate() {
        $date = new \DateTime($this->Ends);
        return $date->format('Y-m-d');
    }

    public function getEnds() {
        return $this->Ends;
    }
     
    public function getDescription() {
        return $this->Description;
    }
     
    public function getOrganiserName() {
        return $this->OrganiserName;
    }
     
    public function getOrganiserDescription() {
        return $this->OrganiserDescription;
    }
     
    public function getEventCategoryId() {
        return $this->EventCategoryId;
    }
    
    public function getEventCategoryName() {
        return $this->EventCategoryId;
    }
     
    public function getEventTopicId() {
        return $this->EventTopicId;
    }
    
    public function getEventTopicName() {
        return $this->EventCategoryId;
    }
    
    public function setProperty($prop, $value)
    {
        if (property_exists(self::class, $prop)) {
            $method = "set$prop";
            $this->$method($value);
        }
    }

    public function getProperty($prop)
    {
        if (property_exists(self::class, $prop)) {
            $method = "get$prop";
            return $this->$method();
        }
    }
    
    public function toArray() {
        return array(
            "Name" => $this->getName(),
            "Location" => $this->getLocation(),
            "Id" => $this->getId(),
            "Image" => $this->getImage(),
            "Starts" => $this->getStarts(),
            "Ends" => $this->getEnds(),
            "Description" => $this->getDescription(),
            "OrganiserName" => $this->getOrganiserName(),
            "OrganiserDescription" => $this->getDescription(),
            "EventCategoryId" => $this->getEventCategoryId(),
            "EventTopicId" => $this->getEventTopicId());
    }
    
    public function arrayToObject($arr)
    {
        foreach ($arr as $key => $value) {
            $this->setProperty($key, $value);
        }
    }
    
}