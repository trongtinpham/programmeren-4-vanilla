<?php
Namespace ModernWays\FricFrac\Model;

use ModernWays\Helpers;

class Country
{
    private $Name;
    private $Code;
    private $Id;
    private $List;
    
    public function setName($value) {
        $this->Name = \ModernWays\Helpers::cleanUpInput($value);
    }
    
    public function setCode($value) {
        $this->Code = \ModernWays\Helpers::cleanUpInput($value);
    }
    
    public function setId($value) {
        $this->Id = \ModernWays\Helpers::cleanUpInput($value);
    }
    
    public function setProperty($prop, $value)
    {
        if (property_exists(self::class, $prop)) {
            $method = "set$prop";
            $this->$method($value);
        }
    }

    public function getProperty($prop)
    {
        if (property_exists(self::class, $prop)) {
            $method = "get$prop";
            return $this->$method();
        }
    }
    
    public function getName() {
        return $this->Name;
    }
    
    public function getCode() {
        return $this->Code;
    }
    
    public function getId() {
        return $this->Id;
    }
    
    public function toArray() {
        return array(
            "Name" => $this->Name,
            "Code" => $this->Code,
            "Id" => $this->Id);
    }
    
    public function arrayToObject($array)
    {
        foreach ($array as $key => $value) {
            $this->setProperty($key, $value);
        }
    }
    
}