<?php

namespace ModernWays\FricFrac\Model;

use ModernWays\Helpers;

class Person
{
    private $Id;
    private $FirstName;
    private $LastName;
    private $Email;
    private $Address1;
    private $Address2;
    private $PostalCode;
    private $City;
    private $Phone1;
    private $Birthday;
    private $Rating;
    private $CountryId;

    public function setId($value)
    {
        $this->Id = Helpers::cleanUpInput($value);
    }

    public function setFirstName($value)
    {
        $this->FirstName = Helpers::cleanUpInput($value);
    }

    public function setLastName($value)
    {
        $this->LastName = Helpers::cleanUpInput($value);
    }

    public function setEmail($value)
    {
        $this->Email = Helpers::cleanUpInput($value);
    }

    public function setAddress1($value)
    {
        $this->Address1 = Helpers::cleanUpInput($value);
    }

    public function setAddress2($value)
    {
        $this->Address2 = Helpers::cleanUpInput($value);
    }

    public function setPostalCode($value)
    {
        $this->PostalCode = Helpers::cleanUpInput($value);
    }

    public function setCity($value)
    {
        $this->City = Helpers::cleanUpInput($value);
    }

    public function setPhone1($value)
    {
        $this->Phone1 = Helpers::cleanUpInput($value);
    }

    public function setBirthday($value)
    {
        $this->Birthday = date('Y-m-d', strtotime($value));
    }

    public function setRating($value)
    {
        $this->Rating = Helpers::cleanUpInput($value);
    }

    public function setCountryId($value)
    {
        $this->CountryId = Helpers::cleanUpInput($value);
    }

    public function getId()
    {
        return $this->Id;
    }

    public function getFirstName()
    {
        return $this->FirstName;
    }

    public function getLastName()
    {
        return $this->LastName;
    }

    public function getEmail()
    {
        return $this->Email;
    }

    public function getAddress1()
    {
        return $this->Address1;
    }

    public function getAddress2()
    {
        return $this->Address2;
    }

    public function getPostalCode()
    {
        return $this->PostalCode;
    }

    public function getCity()
    {
        return $this->City;
    }

    public function getPhone1()
    {
        return $this->Phone1;
    }

    public function getBirthday()
    {
        return $this->Birthday;
    }

    public function getRating()
    {
        return $this->Rating;
    }

    public function getCountryId()
    {
        return $this->CountryId;
    }

    public function getCountryName()
    {
        $country = \ModernWays\FricFrac\Dal\Dal::ReadOne('Country', $this->CountryId);
        return $country['Name'];
    }

    public function setProperty($prop, $value)
    {
        if (property_exists(self::class, $prop)) {
            $method = "set$prop";
            $this->$method($value);
        }
    }

    public function getProperty($prop)
    {
        if (property_exists(self::class, $prop)) {
            $method = "get$prop";
            return $this->$method();
        }
    }

    public function toArray()
    {
        return array(
            'Id' => $this->Id,
            'FirstName' => $this->FirstName,
            'LastName' => $this->LastName,
            'Email' => $this->Email,
            'Address1' => $this->Address1,
            'Address2' => $this->Address2,
            'PostalCode' => $this->PostalCode,
            'City' => $this->City,
            'Phone1' => $this->Phone1,
            'Birthday' => $this->Birthday,
            'Rating' => $this->Rating,
            'CountryId' => $this->CountryId
        );
    }

    public function arrayToObject($arr)
    {
        foreach ($arr as $key => $value) {
            $this->setProperty($key, $value);
        }
    }
}