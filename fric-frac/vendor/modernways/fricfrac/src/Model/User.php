<?php
namespace ModernWays\FricFrac\Model;
use ModernWays\Helpers;

class User
{
    private $Name;
    private $Salt;
    private $HashedPassword;
    private $PersonId;
    private $RoleId;
    private $Id;
    
    public function setName($value)
    {
        $this->Name = Helpers::cleanUpInput($value);
    }
    
    public function setSalt($value)
    {
        $this->Salt = Helpers::cleanUpInput($value);
    }
    
    public function setHashedPassword($value)
    {
        $this->HashedPassword = Helpers::cleanUpInput($value);
    }
    
    public function setPersonId($value)
    {
        $this->PersonId = Helpers::cleanUpInput($value);
    }
    
    public function setRoleId($value)
    {
        $this->RoleId = Helpers::cleanUpInput($value);
    }
    
    public function setId($value)
    {
        $this->Id = Helpers::cleanUpInput($value);
    }
    
    public function getName()
    {
        return $this->Name;
    }
    
    public function getSalt()
    {
        return $this->Salt;
    }
    
    public function getHashedPassword()
    {
        return $this->HashedPassword;
    }
    
    public function getPersonId()
    {
        return $this->PersonId;
    }
    
    public function getPersonName()
    {
        $person = \ModernWays\FricFrac\Dal\Dal::ReadOne('Person', $this->PersonId);
        return $person['FirstName']  . ' ' . $person['LastName'];
    }
    
    public function getRoleId()
    {
        return $this->RoleId;
    }
    
    public function getRoleName()
    {
        $role = \ModernWays\FricFrac\Dal\Dal::ReadOne('Role', $this->RoleId);
        return $role['Name'];
    }
    
    public function getId()
    {
        return $this->Id;
    }
    
    public function setProperty($prop, $value)
    {
        if (property_exists(self::class, $prop)) {
            $method = "set$prop";
            $this->$method($value);
        }
    }

    public function getProperty($prop)
    {
        if (property_exists(self::class, $prop)) {
            $method = "get$prop";
            return $this->$method();
        }
    }
    
    public function toArray()
    {
        return array(
            'Name' => $this->Name,
            'Salt' => $this->Salt,
            'HashedPassword' => $this->HashedPassword,
            'PersonId' => $this->PersonId,
            'RoleId' => $this->RoleId,
            'Id' => $this->Id,
        );
    }
    
    public function arrayToObject($arr)
    {
        foreach ($arr as $key => $value) {
            $this->setProperty($key, $value);
        }
    }
}